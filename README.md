### Java Kafka Sample
# Описание
Данный пример демонстрирует взаимодействие kafka из докера с сервером на Java из докера

# Запуск
1. `docker compose -f kafka-docker-compose.yml up -d`
   запуск kafka-docker-compose.yml с Kafka и provectuslabs/kafka-ui:v0.4.0. По умолчаию включен конфиг
   KRAFT для kafka, который позволяет стартовать Kafka без Zookeeper. **provectuslabs** это тула предоставляющая ui для визуацлизации
   сущностей Kafka(в данном случае ui досупен на порту 8088)
2. `mvn clean package`
   создание jar файла сервера
3. `docker build -t kafka_sample_image .`
   создание билда сервера
4. `docker run --network java-kafka-sample_default -d -p 8080:8080 --name kafka_sample kafka_sample_image`
   `--network java-kafka-sample_default` - в данное опции указывается сеть в которой будет запущен сервер java, благодаря этой настройке сервер может
   поключаться к Kafka по следующему адресу: kafka:9092. Название сети может быть другой, командой `docker network ls` можно посмотерть список доступных сетей
   `--name kafka_sample` - опция, указывающая имя контейнера

# Примечание
   Если запускать сервер вне контейнера и подключаться к kafka, то необходимо заменить значение конфига в application.properties на следующий
   `spring.kafka.bootstrap-servers=localhost:9094`

# Примечание для Java кода
   В коде реализованы сразу и продьюсер и консьюмер. Работают только с одним топиков, состоящий из одной партиции. 
   Метод `sendMessage()` - продьюсер, метод `listenSingleTopic()` - консьюмер в классе Application

# Примечание для инстанса Kafka Bitnami
   1. Необходимые настройки контейнера Kafka
   * KAFKA_CFG_LISTENERS - регистрирует список слушателей, через запятую. Даная настройка позволяет подключаться к кафка
   * KAFKA_CFG_ADVERTISED_LISTENERS - это слушатели по сути возможносмть сообщить клиентам(производителям или поьребителям) как они могут связаться с брокером
                                    Kafka и картой протокола безопасности. Важно понимать что этот конфиг зависит  от этого KAFKA_CFG_LISTENERS. В том случае, если 
                                    будут прописаны неправильно адрес конкретных слушателей, то юзер, например, сможет подключиться к Kafka, но не сможет выполнять
                                    какие-либо действия
   * KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP - сообщить клиентам о протоколах безопасности, которые следует использовать при подключении к Kafka. В
                                    KAFKA_LISTENER_SECURITY_PROTOCOL_MAP мы сопоставляем имена наших пользовательских протоколов с действующими протоколами
                                    безопасности
   2. Подключение к Kafka с другого хоста
      * Добавление нового слушателя  KAFKA_CFG_LISTENERS=EXTERNAL_DIFFERENT_HOST=://9093, ...
      * Добавление нового ADVERTISED слушателя KAFKA_CFG_LISTENERS=EXTERNAL_DIFFERENT_HOST=://broker0.company.com:9093,
      * Добавить еще один порт наружу из докера 9093:9093
      
   _**Имя слушателя может быть любое, но они должны совпадать, то есть в KAFKA_CFG_ADVERTISED_LISTENERS и в KAFKA_CFG_LISTENERS должны использоваться одни и те
      же имена слушателей(например INTERNAL, EXTERNAL, EXTERNAL_DIFFERENT_HOST). Чаще всего необходимо описать как минимум двух слушателей - INTERNAL, 
      для подключение к Kafka в той же сети Docker, EXTERNAL_DIFFERENT_HOST, для подключение к Kafka находящемуся на другом хосте**_
# Источники
   1. https://www.youtube.com/watch?v=L--VuzFiYrM
   2. https://habr.com/ru/companies/otus/articles/670440/
   3. https://www.baeldung.com/spring-kafka
