package com.sergey.kafka_sample;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@SpringBootApplication
@RestController
public class Application implements ApplicationRunner {


	private final KafkaTemplate<String, String> kafkaTemplate;
//	private final NewTopic topicName;

	public Application(KafkaTemplate<String, String> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}


	@KafkaListener(topics = "sergey1")
	public void listenSingleTopic(String message) {
		System.out.println("Received Message in group foo: " + message);
	}
	@Override
	public void run(ApplicationArguments args) throws Exception {
		for (int i = 0; i < 10; i++) {
			sendMessage("test_" + i);
		}

	}

	public void sendMessage(String message) {
		CompletableFuture<SendResult<String, String>> future = kafkaTemplate.send("sergey1", message);
		future.whenComplete((result, ex) -> {
			if (ex == null) {
				System.out.println("Sent message=[" + message +
						"] with offset=[" + result.getRecordMetadata().offset() + "]");
			} else {
				System.out.println("Unable to send message=[" +
						message + "] due to : " + ex.getMessage());
			}
		});
	}
}