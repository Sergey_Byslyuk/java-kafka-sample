FROM openjdk:17-jdk-slim
WORKDIR /app

COPY target/kafka_sample-0.0.1-SNAPSHOT.jar  .

EXPOSE 8080

CMD ["java", "-jar", "kafka_sample-0.0.1-SNAPSHOT.jar"]